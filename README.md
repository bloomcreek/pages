# Map of bloomcreek

Feel free to wander, but here is a map in case you get lost. Everywhere you go there should be a link to this map at the top of the page.

## [robots.txt](./robots.txt)

Disallows spiderbots from crawling around here

## [About bloomcreek](./about.html)

To learn more information about this place

## [Current](./current.html)

For anyone who wants to know how my life is currently going

## [Notes](./notes/index.html)

The garden for my notes on various topics

## [Recipes](./recipes/index.html)

Recipes that I enjoy or would like to make
